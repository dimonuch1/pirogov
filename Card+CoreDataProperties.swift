//
//  Card+CoreDataProperties.swift
//  test_16
//
//  Created by kira on 16.03.2020.
//  Copyright © 2020 homecat116. All rights reserved.
//
//

import Foundation
import CoreData


extension Card {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Card> {
        return NSFetchRequest<Card>(entityName: "Card")
    }

    @NSManaged public var number: Int64
    @NSManaged public var id: UUID?
    @NSManaged public var holder: Person?

}
