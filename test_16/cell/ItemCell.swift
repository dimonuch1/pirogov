//
//  ItemCell.swift
//  test_16
//
//  Created by kira on 16.03.2020.
//  Copyright © 2020 homecat116. All rights reserved.
//

import UIKit

class ItemCell: UITableViewCell {

    static let identifire = "ItemCell"

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var idLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func config(person: Person?, card: [Card]) {
        if let person = person {
            typeLabel.text = "Person"
            typeLabel.textColor = .green
            title.text = person.name

            if let id = person.id {
                idLabel.text = "\(id)"
            } else {
                idLabel.text = "none"
            }
        } else if !card.isEmpty {
            typeLabel.text = "Card"
            typeLabel.textColor = .red
            title.text = "Card"

            if let id = card.randomElement()?.id {
                idLabel.text = "\(id)"
            } else {
                idLabel.text = "none"
            }
        } else {
            typeLabel.text = "Card"
            typeLabel.textColor = .red
            title.text = "none"
            idLabel.text = "none"
        }
    }
    
}
