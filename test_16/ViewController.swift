//
//  ViewController.swift
//  test_16
//
//  Created by kira on 16.03.2020.
//  Copyright © 2020 homecat116. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
// MARK: - IBOutlt
    @IBOutlet weak var tableView: UITableView!

    // MARK: - Properties
    var persons = [Person]()
    var cards = [Card]()

    var dateFormatter = DateFormatter()

    var moc: NSManagedObjectContext!
    var privateManagedObjectContext: NSManagedObjectContext!

    let appDelegate = UIApplication.shared.delegate as? AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.dataSource = self
        tableView.register(UINib(nibName: "ItemCell", bundle: nil),
                           forCellReuseIdentifier: ItemCell.identifire)

        moc = appDelegate?.persistentContainer.viewContext
        privateManagedObjectContext = appDelegate?.persistentContainer.newBackgroundContext()

        deleteAllEntities()
        createItems()
    }

    func loadData() {
        let personRequest: NSFetchRequest<Person> = Person.fetchRequest()
        let cardRequest: NSFetchRequest<Card> = Card.fetchRequest()
        do {
            try persons = privateManagedObjectContext.fetch(personRequest)
            try cards = privateManagedObjectContext.fetch(cardRequest)
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        } catch {
            print("Could not load data")
        }
    }

}

// MARK: - create objects

extension ViewController {
    private func createItems() {

        let serialPriorityQueue2 = DispatchQueue(label: "com.test", qos: .background)

        serialPriorityQueue2.async {
            for i in 0...1000 {
            print("create number: \(i)")
                let person = Person(context: self.privateManagedObjectContext)
                person.name = "\(i)"
                person.id = UUID()

                let card = Card(context: self.privateManagedObjectContext)
                card.id = UUID()
                person.addToCards(card)

                self.appDelegate?.saveContext()
            }

            DispatchQueue.main.async {
                let alert = UIAlertController(title: "Create items", message: "creating is done", preferredStyle: .alert)
                let alertAction = UIAlertAction(title: "ok", style: .default) { (action) in
                    DispatchQueue(label: "", qos: .background).async {
                        self.loadData()
                    }
                }

                alert.addAction(alertAction)
                self.present(alert, animated: true, completion: nil)
            }
        }

    }
}

// MARK: - deleting entities
extension ViewController {
    private func deleteAllEntities() {
        deleteEntity(enityName: "Person")
        deleteEntity(enityName: "Card")
    }

    private func deleteEntity(enityName: String) {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: enityName)
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)

        do {
            try moc.execute(batchDeleteRequest)

        } catch {
            // Error Handling
        }
    }
}

// MARK: - UITableViewDataSource
extension ViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return persons.isEmpty ? 0 : (persons.count / 2)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: ItemCell.identifire) as? ItemCell {
            if indexPath.row % 2 == 0 {
                cell.config(person: persons[indexPath.row], card: [])
            } else {
                if let cards = persons[indexPath.row].cards?.array as? [Card] {
                    cell.config(person: nil, card: cards)
                } else {
                    cell.config(person: nil, card: [])
                }
            }

            return cell
        }
        return UITableViewCell()
    }

}
